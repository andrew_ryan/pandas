fn main() {
    demo();
}

fn demo() {
    use pandas_rs::prelude::*;
    let df = Pd::read_csv("./dataset/rows.csv").unwrap(); //read csv file
    let col = Pd::get_column_by_index(&df, 3); //return sum of column values
    col.display();
    let col = Pd::get_column(&df, "Station Latitude"); //return sum of column values
    let row = Pd::get_row_by_index(&df, 0); //return the index of rows
    row.display();
    let sum_col = Pd::sum_column(&df, "Station Latitude"); //return sum of column values
    let sum_row = Pd::sum_row(&df, 0); //return sum of column values
    let unique = Pd::unique(&col);
    unique.display();
    // Delete all rows with missing values
    let new_df = Pd::dropna(&df, "any");
    new_df.display();
    Pd::save_csv(df.clone(), "./new_df.csv"); //save Vec<Vec<STring>> to csv file
    println!("after dropna:{:?}", &new_df.len()); // after dropna operation
    println!("head:{:?}", Pd::head(&df, 1)); //return the head 5 element of csv_vec
    println!("tail:{:?}", Pd::tail(&df, 1)); //return the from tail 5 element of csv_vec
    println!("{:?}", sum_col); // return sum of  the column "Station Latitude"
    println!("{:?}", sum_row); // return sum of  the column "Station Latitude"
}
