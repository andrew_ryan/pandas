#[allow(warnings)]
pub mod prelude {
    pub trait Cloneable {
        fn clone_by_index(&self, index: usize) -> String;
    }
    impl Cloneable for Vec<String> {
        fn clone_by_index(&self, index: usize) -> String {
            let s = self.to_owned();
            format!("{}", &s[index])
        }
    }

    pub trait CloneableVec {
        fn clone_vec_by_index(&self, index: usize) -> Vec<String>;
    }
    impl CloneableVec for Vec<Vec<String>> {
        fn clone_vec_by_index(&self, index: usize) -> Vec<String> {
            let s = self.to_owned();
            let ss = s.get(index).unwrap();
            ss.to_vec()
        }
    }
    pub trait Display {
        fn display(&self) -> ();
    }
    pub trait AsByte {
        fn as_byte(&self) -> Vec<u8>;
    }

    
    impl AsByte for Vec<Vec<String>> {
        fn as_byte(&self) -> Vec<u8> {
            let vec = self.clone();
            let vec_string = vec.iter().map(|s|{s.join(",")}).collect::<Vec<String>>().join("\n");
            let u8_vec = vec_string.as_bytes();
            u8_vec.to_owned()
        }
    }

    impl Display for Vec<Vec<String>> {
        fn display(&self) {
            use cli_table::{format::Border, format::Justify, print_stdout, Cell, Table};
            let table = self
                .iter()
                .map(|s| {
                    s.iter()
                        .map(|x| x.cell().justify(Justify::Center))
                        .collect::<Vec<_>>()
                })
                .collect::<Vec<_>>()
                .table()
                .border(Border::default());

            assert!(print_stdout(table).is_ok());
        }
    }
    impl Display for Vec<String> {
        fn display(&self) {
            let vec_vec_self = self.iter().map(|s| vec![s.to_owned()]).collect::<Vec<_>>();
            vec_vec_self.display();
        }
    }
    
    use std::fs;
    use useful_macro::*;
    pub struct Pd {}
    pub type Err = Box<dyn std::error::Error>;
    impl Pd {
        pub fn read_csv(path: &str) -> Result<Vec<Vec<String>>, std::io::Error> {
            // let mut re = vec![];
            match fs::read(path) {
                Ok(r) => {
                    let rows_vec = split_to_vec!(&String::from_utf8_lossy(&r), "\n");
                    let each_row_vec = rows_vec
                        .into_iter()
                        .map(|s| {
                            let re = split_to_vec!(&s, ",");
                            let re_vec = re
                                .clone()
                                .into_iter()
                                .map(|s| {
                                    let re = s.trim().replace("\"", "");
                                    re.to_owned()
                                })
                                .collect::<Vec<_>>();
                            re_vec
                        })
                        .collect::<Vec<Vec<_>>>();
                    Ok(each_row_vec)
                }
                Err(e) => Err(e),
            }
            // Ok(re)
        }
        pub fn save_csv(contents:Vec<Vec<String>>,path:impl ToString){
            use std::fs;
            fs::write(path.to_string(), contents.as_byte()).unwrap()
        }
        pub fn head(csv_vec: &Vec<Vec<String>>, head_size: usize) -> Vec<Vec<String>> {
            let mut re = vec![];
            if &csv_vec.len() > &head_size {
                for i in 0..head_size {
                    re.push(vec_element_clone!(csv_vec, i));
                }
            } else {
                re = csv_vec.to_owned();
            }
            re
        }
        pub fn tail(csv_vec: &Vec<Vec<String>>, tail_len: usize) -> Vec<Vec<String>> {
            let mut re = vec![];
            if &csv_vec.len() > &tail_len {
                for i in 0..tail_len {
                    re.push(vec_element_clone!(csv_vec, &csv_vec.len() - i - 1));
                }
            } else {
                re = csv_vec.to_owned();
            }
            re
        }
        pub fn get_column_by_index(csv_vec: &Vec<Vec<String>>, column_index: usize) -> Vec<String> {
            let csv_vec_0: Vec<String> = csv_vec.clone_vec_by_index(0);
            let mut index = 0;
            for (i, v) in csv_vec_0.clone().into_iter().enumerate() {
                if &i == &column_index {
                    index = i;
                }
            }
            let mut re = vec![];
            for (i, v) in csv_vec.into_iter().skip(1).enumerate() {
                re.push(vec_element_clone!(v, index));
            }
            re
        }
        pub fn get_column(csv_vec: &Vec<Vec<String>>, column_str: &str) -> Vec<String> {
            let csv_vec_0: Vec<String> = vec_element_clone!(csv_vec, 0);
            let mut index = 0;
            for (i, v) in csv_vec_0.clone().into_iter().enumerate() {
                if &v == column_str {
                    index = i;
                }
            }
            let mut re = vec![];
            for (i, v) in csv_vec.into_iter().skip(1).enumerate() {
                re.push(vec_element_clone!(v, index));
            }
            re
        }
        pub fn get_row_by_index(csv_vec: &Vec<Vec<String>>, row_index: usize) -> Vec<String> {
            vec_element_clone!(csv_vec, row_index)
        }
        pub fn sum_column(csv_vec: &Vec<Vec<String>>, column_str: &str) -> f64 {
            let sum_column = Pd::get_column(csv_vec, column_str);
            let sum = vec_element_parse!(sum_column, f64);
            sum.into_iter().sum::<f64>()
        }
        pub fn sum_row(csv_vec: &Vec<Vec<String>>, row_index: usize) -> f64 {
            let sum_column = Pd::get_row_by_index(csv_vec, row_index);
            let sum = vec_element_parse!(sum_column, f64);
            sum.into_iter().sum::<f64>()
        }
        pub fn unique(vec: &Vec<String>) -> Vec<String> {
            let mut v = vec.clone();
            deduped_sorted!(v)
        }
        pub fn dropna(csv_vec: &Vec<Vec<String>>, how: &str) -> Vec<Vec<String>> {
            let mut re = vec![];
            if csv_vec.len() > 1 && how == "any" {
                let first_row_len = csv_vec[0].len();
                let mut remove_vec = vec![];
                for i in 1..csv_vec.len() {
                    if csv_vec[i].len() != first_row_len {
                        remove_vec.push(i);
                    }
                }
                let mut v = csv_vec
                    .iter()
                    .enumerate()
                    .filter(|(i, v)| !remove_vec.contains(i))
                    .map(|(i, v)| v.to_owned())
                    .collect::<Vec<_>>();
                re = v;
            } else if csv_vec.len() > 1 && how == "all" {
            }
            re
        }
    }

    fn is_csv_complate(csv_vec: Vec<Vec<String>>) -> bool {
        let mut re_vec: Vec<bool> = vec![&csv_vec.len() > &0];
        csv_vec.iter().map(|s| {
            re_vec.push(&csv_vec[0].len() == &s.len());
        });
        if re_vec.contains(&false) {
            return false;
        } else {
            return true;
        }
    }

    fn is_parseable(s: &str) -> bool {
        match s.parse::<f64>() {
            Ok(_r) => return true,
            Err(_e) => return false,
        }
    }
}
